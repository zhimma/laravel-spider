<?php

namespace App\Console\Commands\Spider;

use Illuminate\Console\Command;
use Spatie\Browsershot\Browsershot;
use Symfony\Component\DomCrawler\Crawler;

class Short extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spider:short';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function handle()
    {
        $crawler = new Crawler();
        $html = Browsershot::url('https://www.youtube.com/@foodtimedaole/shorts')
            ->touch(true)
            ->setRemoteInstance('127.0.0.1', 9222)
            ->setChromePath("/Applications/Google Chrome.app")
            ->setDelay(2000)
            ->setOption('proxy', 'http://127.0.0.1:33210')
            ->bodyHtml();


        $crawler->addHtmlContent($html);
        $result = [];
        $crawler->children("body > ytd-app > div")->first()->filter('#contents > ytd-rich-grid-row')->each(
            function (Crawler $node) use (&$result) {
                $result[] = [
                    // 'html' => $node->filter('#thumbnail > yt-image')->html(),
                    // 'image' => $node->filter('#thumbnail > yt-image > img')->attr('src'),
                    'title' => $node->filter('span[id="video-title"]')->text(),
                    'view_count' => $node->filter(
                        'span[class="inline-metadata-item style-scope ytd-video-meta-block"]'
                    )->text(),
                    'link' => $node->filter(
                        'a[class="yt-simple-endpoint focus-on-expand style-scope ytd-rich-grid-slim-media"]'
                    )->attr("href"),
                ];
            }
        );
        dd($result);
    }
}
