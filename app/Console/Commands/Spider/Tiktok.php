<?php

namespace App\Console\Commands\Spider;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Spatie\Browsershot\Browsershot;
use Symfony\Component\DomCrawler\Crawler;


class Tiktok extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spider:tiktok';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function handle()
    {
        $opts = [
            // 设置http代理
            'proxy' => 'http://127.0.0.1:33210',
        ];

//        curl 'https://www.tiktok.com/api/user/detail/?WebIdLastTime=1699002766&aid=1988&app_language=zh-Hans&app_name=tiktok_web&browser_language=zh-CN&browser_name=Mozilla&browser_online=true&browser_platform=MacIntel&browser_version=5.0%20%28Macintosh%3B%20Intel%20Mac%20OS%20X%2010_15_7%29%20AppleWebKit%2F537.36%20%28KHTML%2C%20like%20Gecko%29%20Chrome%2F120.0.0.0%20Safari%2F537.36%20Edg%2F120.0.0.0&channel=tiktok_web&cookie_enabled=true&device_id=7297161254648923650&device_platform=web_pc&focus_state=true&from_page=user&history_len=3&is_fullscreen=true&is_page_visible=true&language=zh-Hans&os=mac&priority_region=&referer=&region=TW&screen_height=1050&screen_width=1680&secUid=MS4wLjABAAAAgANEBD7F8Zmowz-dX7AzTYFm_Mk6-et3aSgcYdqGsBXfOmW-siCaBUlvx1kN1gdN&tz_name=Asia%2FShanghai&uniqueId=brentwithbenefits94&verifyFp=verify_lqanhroi_armdp825_e8qz_4j54_8rKz_ZMEkbbgxHrVv&webcast_language=zh-Hans&msToken=0b-RQ_7bU4o674WbqYM5XvzJuwpX3-zWrBp350Mk9A1DjHd6F5orEO00xZXn7vm95JC4gDTdHewfDQzoed9n0TUJsuKIhE5DT-uUDQdusiXrLsgSATNcRwBLudg=&X-Bogus=DFSzswVO7zsANGIJtN3pP09WcBrs&_signature=_02B4Z6wo00001QTzfDAAAIDBBPN8MkB94R0E83iAACS.c1' \
//  -H 'authority: www.tiktok.com' \
//  -H 'accept: */*' \
//  -H 'accept-language: zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6' \
//  -H 'cache-control: no-cache' \
//  -H 'cookie: tt_chain_token=uFQwNim6p/ti1uVWHI+cwg==; tiktok_webapp_theme=light; passport_csrf_token=815e3afd71dfc006ef7846323d40c27d; passport_csrf_token_default=815e3afd71dfc006ef7846323d40c27d; msToken=0b-RQ_7bU4o674WbqYM5XvzJuwpX3-zWrBp350Mk9A1DjHd6F5orEO00xZXn7vm95JC4gDTdHewfDQzoed9n0TUJsuKIhE5DT-uUDQdusiXrLsgSATNcRwBLudg=; multi_sids=; odin_tt=5168882542d4a269359826aa912dfb84f6195efb8dee496ce85fd58bbaf2e05c1097d4834e4d36961157398620667d4750e41d8de6dcb2da300152cb2a3787e6c430dd871805d45ed8a9eeca01e73999; tt_csrf_token=PeHLN0hn-ExdEHs-GLeufS0v7UcJhy9FahkU; __tea_cache_tokens_1988={%22_type_%22:%22default%22%2C%22user_unique_id%22:%227297161254648923650%22%2C%22timestamp%22:1699003576346}; s_v_web_id=verify_lqanhroi_armdp825_e8qz_4j54_8rKz_ZMEkbbgxHrVv; perf_feed_cache={%22expireTimestamp%22:1703059200000%2C%22itemIds%22:[%227303728609955515653%22%2C%227286452993673694472%22%2C%227292231803731987714%22]}; msToken=QotWC7uDSGN-ADko4_cG_lBGT-Cs5-xGF2Lit7dKP_lEfV3P0onbhRdr7JlsUGgl7Zkya0eiAaABm4kj9Em2W26Ydo2oK5zNUMB7nCSjqq7gNbERnIu-VDQqkwI=; ext_pgvwcount=0.9; ttwid=1%7CPyP6TDl9IqNTI9ZDE3TNoNhijQRqOZDWTCm6yd6xh3Y%7C1702889410%7C30bf488921f7d6c5e6b8591e27b0d217edbc0c6b4136cfbc2e0ae85aa59665ee' \
//  -H 'dnt: 1' \
//  -H 'pragma: no-cache' \
//  -H 'referer: https://www.tiktok.com/@brentwithbenefits94' \
//  -H 'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Microsoft Edge";v="120"' \
//  -H 'sec-ch-ua-mobile: ?0' \
//  -H 'sec-ch-ua-platform: "macOS"' \
//  -H 'sec-fetch-dest: empty' \
//  -H 'sec-fetch-mode: cors' \
//  -H 'sec-fetch-site: same-origin' \
//  -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 Edg/120.0.0.0' \
//  --compressed
        $response = Http::withOptions([
            'proxy' => 'http://127.0.0.1:33210',
        ])
            ->withHeaders([
                'accept'             => '*/*',
                'accept-language'    => 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
                'cache-control'      => 'no-cache',
                'dnt'                => '1',
                'pragma'             => 'no-cache',
                'referer'            => 'https://www.tiktok.com/@brentwithbenefits94',
                'sec-ch-ua'          => '"Not_A Brand";v="8", "Chromium";v="120", "Microsoft Edge";v="120"',
                'sec-ch-ua-mobile'   => '?0',
                'sec-ch-ua-platform' => '"macOS"',
            ])
            ->get(
                'https://www.tiktok.com/api/user/detail/',
                [
                    'browser_platform' => 'MacIntel',
                    'browser_version'  => '5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 Edg/120.0.0.0',
                    'channel'          => 'tiktok_web',
                    'msToken'          => '0b-RQ_7bU4o674WbqYM5XvzJuwpX3-zWrBp350Mk9A1DjHd6F5orEO00xZXn7vm95JC4gDTdHewfDQzoed9n0TUJsuKIhE5DT-uUDQdusiXrLsgSATNcRwBLudg=',
                    'X-Bogus'          => 'DFSzswVOnRGANGIJtN36vz9WcBjP',
                    'cookie_enabled'   => 'true',
                    '_signature'       => '_02B4Z6wo00001lwjZDgAAIDCXCNkO5X8fi5cI2CAAPKXe2',
                    'device_id'        => '7297161254648923650',
                    'region'           => 'TW',
                    'device_platform'  => 'web_pc',
                    'WebIdLastTime'    => time(),
                    'focus_state'      => false,
                    'aid'              => 1988,
                    'from_page'        => 'user',
                    'secUid'           => 'MS4wLjABAAAAgANEBD7F8Zmowz-dX7AzTYFm_Mk6-et3aSgcYdqGsBXfOmW-siCaBUlvx1kN1gdN',
                    'app_language'     => 'zh-Hans',
                    'history_len'      => 3,
                    'tz_name'          => 'Asia/Shanghai',
                    'app_name'         => 'tiktok_web',
                    'uniqueId'         => 'brentwithbenefits94',
                    'browser_language' => 'zh-CN',
                    'is_page_visible'  => 'true',

                ]
            );

        $requests = Browsershot::url('https://www.tiktok.com/@brentwithbenefits94')
            ->triggeredRequests();

        foreach ($requests as $request) {
            if (strpos($request['url'], "https://www.tiktok.com/api/post/item_list/") !== false) {
                $body = Http::withOptions([
                    'proxy' => 'http://127.0.0.1:33210',
                    "debug" => true
                ])->withHeaders([
                    'authority: www.tiktok.com',
                    'accept: */*',
                    'accept-language: zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
                    'cache-control: no-cache',
                    'cookie: tt_chain_token=uFQwNim6p/ti1uVWHI+cwg==; tiktok_webapp_theme=light; passport_csrf_token=815e3afd71dfc006ef7846323d40c27d; passport_csrf_token_default=815e3afd71dfc006ef7846323d40c27d; msToken=0b-RQ_7bU4o674WbqYM5XvzJuwpX3-zWrBp350Mk9A1DjHd6F5orEO00xZXn7vm95JC4gDTdHewfDQzoed9n0TUJsuKIhE5DT-uUDQdusiXrLsgSATNcRwBLudg=; multi_sids=; odin_tt=5168882542d4a269359826aa912dfb84f6195efb8dee496ce85fd58bbaf2e05c1097d4834e4d36961157398620667d4750e41d8de6dcb2da300152cb2a3787e6c430dd871805d45ed8a9eeca01e73999; tt_csrf_token=PeHLN0hn-ExdEHs-GLeufS0v7UcJhy9FahkU; __tea_cache_tokens_1988={%22_type_%22:%22default%22%2C%22user_unique_id%22:%227297161254648923650%22%2C%22timestamp%22:1699003576346}; tta_attr_id=0.1702891157.7313861830022004738; tta_attr_id_mirror=0.1702891157.7313861830022004738; s_v_web_id=verify_lqanhroi_armdp825_e8qz_4j54_8rKz_ZMEkbbgxHrVv; ttwid=1%7CPyP6TDl9IqNTI9ZDE3TNoNhijQRqOZDWTCm6yd6xh3Y%7C1702976578%7C9133b49ba1701447a0070e88ffb0d0b599bf8c9d19e5380fe34c2e8d1f248879; ext_pgvwcount=1; perf_feed_cache={%22expireTimestamp%22:1703149200000%2C%22itemIds%22:[%227286276401173400838%22%2C%227283321645144493330%22%2C%227304192801254706450%22]}; msToken=PTzwIAIelY5_S1IetoApZxoZ2BG8hupqkDAOl4Jgbh-4lJytBuYg7vzz12DIMSCrskdhIEy_RbF8fy29NyjrnsbyGrtalbCF6pL2TV61PSPWyhnU1mUiSE657eM=',
                    'dnt: 1',
                    'pragma: no-cache',
                    'referer: https://www.tiktok.com/@brentwithbenefits94',
                    'sec-ch-ua: "Not_A Brand";v="8", "Chromium";v="120", "Microsoft Edge";v="120"',
                    'sec-ch-ua-mobile: ?0',
                    'sec-ch-ua-platform: "macOS"',
                    'sec-fetch-dest: empty',
                    'sec-fetch-mode: cors',
                    'sec-fetch-site: same-origin',
                    'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 Edg/120.0.0.0',
                ])->get($request['url'])->body();
                dd($body);
            }
        }
    }

    public function getUserInfo()
    {
        $crawler = new Crawler();
        $html = Browsershot::url('https://www.tiktok.com/@brentwithbenefits94')
            ->touch(true)
            ->setDelay(2000)
            ->setOption('proxy', 'http://127.0.0.1:33210')
            ->bodyHtml();


        $crawler->addHtmlContent($html);
        $scriptElement = $crawler->filterXPath(
            '//script[@id="__UNIVERSAL_DATA_FOR_REHYDRATION__" and @type="application/json"]'
        );
        $content = $scriptElement->text();
        $data = json_decode($content, true);
        dd($data['__DEFAULT_SCOPE__']);
    }

}
