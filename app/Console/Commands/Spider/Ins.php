<?php

namespace App\Console\Commands\Spider;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use QL\QueryList;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

class Ins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spider:ins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function handle()
    {
        $opts = [
            // 设置http代理
            'proxy' => 'http://127.0.0.1:33210',
        ];
       /* $ql = QueryList::get('https://www.instagram.com/api/graphql?av=17841462499264311&__d=www&__user=0&__a=1&__req=1d&__hs=19667.HYP%3Ainstagram_web_pkg.2.1..0.1&dpr=2&__ccg=UNKNOWN&__rev=1009706645&__s=%3Acdw7nl%3Ae4jumx&__hsi=7298265397758124948&__dyn=7xeUmwlEnwn8K2WnFw9-2i5U4e1ZyUW3qi2K360CEbotw50x609vCwjE1xoswIwuo2awt81s8hwGwQwoEcE7O2l0Fwqo31w9O7U2cxe0EUjwGzEaE7622362W2K0zK5o4q3y1Sx_w4HwJwSyES1Twoob82cwMwrUdUco5B0oo5C1Iwqo5q1IGp1yUow&__csr=gqNIix4zi4jRuDkCQznl7FkHFfKViSqiF26_Tp9lzaWiAACox3o8F_yaCXmah5-ESmqhpplhVGgKKeWGcJd1apVFExooCAK3yEjACw04uTg8i0de10zJw1AW06w1-0Kod62m099w5vw72401HOfw0tP8&__comet_req=7&fb_dtsg=NAcO5McSi-G2WMu5-3G1g_hhTU8M5Meimuw1wfs1gRROZ23m1ChiiRA%3A17843691127146670%3A1699258213&jazoest=26031&lsd=mYl-YtRrDFxXUHrRiymNAe&__spin_r=1009706645&__spin_b=trunk&__spin_t=1699259830&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PolarisClipsTabRootPaginationQuery&variables=%7B%22after%22%3A%22GvYciIqe0pDAg4ZZitT9uqWO3f5YjMLSrNXWmKVZkLScs6H5_bFZqJyakprv2eRYrsbsmv32vYZZtMiRivXCjpVZvvzV_Pu9tOBYyJyR6tXhx4tZ0uaK45Tft6VZ1KrYy7Tp3vlY6q6Tk6eKuotZ8P7hkrD4-JdYhKf7qrzquthYjpu0i6DfqaJYlsHU47jaud1YmJeWs5m5o_ZYoreIuqaUlZtZppmlq_-h2ZpYqMeP-q2fpMZYrov14uaY8pFZsv28sPqY4vFYtOWX6t61_8JYytn98pqAsvFY0JXzi96-oflY3o-gk-fC5KRY7Onxmquf8MhY_O3-wr-Hzc5ZJoDFurz0YhQ4NAgpCBgAGggYDXM6MThiYTNjNzUxNDAqBhkMAA%3D%3D%22%2C%22before%22%3Anull%2C%22data%22%3A%7B%22container_module%22%3A%22clips_viewer_clips_tab%22%2C%22seen_reels%22%3A%22%5B%7B%5C%22id%5C%22%3A%5C%223206204739443209477%5C%22%7D%2C%7B%5C%22id%5C%22%3A%5C%223195433308132436418%5C%22%7D%2C%7B%5C%22id%5C%22%3A%5C%223180888286871946223%5C%22%7D%2C%7B%5C%22id%5C%22%3A%5C%223228687436446948222%5C%22%7D%2C%7B%5C%22id%5C%22%3A%5C%223191047164972120694%5C%22%7D%2C%7B%5C%22id%5C%22%3A%5C%223198879258883868436%5C%22%7D%2C%7B%5C%22id%5C%22%3A%5C%223217031774308290694%5C%22%7D%5D%22%2C%22chaining_media_id%22%3A%22Cyx--ULM40I%22%2C%22should_refetch_chaining_media%22%3Afalse%7D%2C%22first%22%3A10%2C%22last%22%3Anull%7D&server_timestamps=true&doc_id=6752172151529745' , "" , $opts);
        dump($ql);*/

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://www.instagram.com',
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);


        $content = $client->request('POST', 'api/graphql' , [
            'headers' => [
                'Accept'     => '*/*',
                'Accept-Encoding'      => "gzip, deflate, br",
                'Accept-Language'      => "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
                'Content-Length'      => "1972",
                'Content-Type'      => "application/x-www-form-urlencoded",
                'Dnt'      => "1",
                'Dpr'      => "2",
                'Origin'      => "https://www.instagram.com",
                'Referer'      => "https://www.instagram.com",
                'User-Agent'      => "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/118.0.0.0",
                'X-Asbd-Id' => '129477',
                'X-Csrftoken' => 'v7jRx85z6cQwQT3HAP9zCCpOjEZi5lGb',
                'X-Fb-Friendly-Name' => 'PolarisClipsTabRootPaginationQuery',
                'X-Fb-Lsd' => 'mYl-YtRrDFxXUHrRiymNAe',
                'X-Ig-App-Id' => '1217981644879628',
                'Cookie'      => 'ig_did=468135AD-0060-41EF-B698-B93048CC9B28; datr=wLdEZQNd5NFtxFjdwsQU1sle; mid=ZUifWgAEAAF_QpY4xdMJ4YDZzNSS; ig_nrcb=1; csrftoken=v7jRx85z6cQwQT3HAP9zCCpOjEZi5lGb; ds_user_id=62594491308; sessionid=62594491308%3Acg80LnFfr5fMsv%3A15%3AAYd5GpSF20cCjIU5ks18Dqdl3coUBw_LbDAG6s9UYQ; dpr=3; rur="NHA\05462594491308\0541730795856:01f7838196f0b70876de640e8ae56b1e17c0d70692e42ac59bdb3710036cef33684125fd"',
            ]
        ]);
        dd($client->getBody()->getContents());

    }
}
