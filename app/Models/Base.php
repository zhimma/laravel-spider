<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Base
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Base newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Base newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Base onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Base query()
 * @method static \Illuminate\Database\Eloquent\Builder|Base withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Base withoutTrashed()
 * @mixin \Eloquent
 */
class Base extends Model
{
    use HasFactory;
    /**
     * 使用软删除
     */
    use SoftDeletes;

    /**
     * 需要被转换日期时间格式的字段
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];
}
