<?php
namespace App\Client;

use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\BrowserKit\Response;

class Client extends AbstractBrowser
{
    protected function doRequest($request): Response
    {
        $content = file_get_contents(__DIR__.'/../fixtures/'.$request->getUri());
        $status = 200;
        $headers = ['Content-Type' => 'text/html'];

        return new Response($content, $status, $headers);
    }
}
