<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

class JWTAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $token = $request->header('Authorization', $request->get('token'));
        // check if header exists
        if (empty($token)) {
            throw new UnauthorizedException('authorization header not found');
        }

        // check if bearer token exists
        if (!preg_match('/Bearer\s(\S+)/', $_SERVER['HTTP_AUTHORIZATION'], $matches)) {
            throw new UnauthorizedException('token not found');
        }

        // extract token
        $jwt = $matches[1];
        if (!$jwt) {
            throw new UnauthorizedException('could not extract token');
        }
        $secretKey = env('JWT_SECRET');
        try {
            $credentials = JWT::decode($jwt, new Key($secretKey, env("JWT_ALGO")));
            // 拿到用户id
            $request->macro("userInfo", function () use ($credentials) {
                return json_decode($credentials->sub, true);
            });
        } catch (\Exception $e) {
            Log::error("unauthorized", [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);

            throw new UnauthorizedException('unauthorized');
        }

        return $next($request);
    }
}
