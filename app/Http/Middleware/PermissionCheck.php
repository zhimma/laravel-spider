<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

class PermissionCheck
{

    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $userInfo = $request->userInfo() ?? [];
        // 查询用户信息及权限 进行权限验证
        if ($userInfo['is_super']) {
            return $next($request);
        }
        $method = strtoupper($request->method());
        // 简单的根据请求类型验证 还有其他很多方式灵活切换 ，普通管理员只允许查看
        if (in_array($method, ['POST', 'DELETE', 'PUT'])) {
            return \response()->json(['status' => -1, 'message' => "暂无权限"], 401);
        }

        return $next($request);
    }
}
