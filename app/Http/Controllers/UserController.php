<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $this->validate($request, [
            'email' => 'filled|email',
            'name' => 'filled|string'
        ]);
        $list = Users::query()
            ->when(!empty($params['email']), function ($query) use ($params) {
                $query->where('email', $params['email']);
            })
            ->when(!empty($params['name']), function ($query) use ($params) {
                $query->where('name', 'like', '%' . $params['email'] . '%');
            })
            ->paginate();
        return response()->json(['status' => 0, 'data' => $list]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Users::query()->find($id);
        return response()->json(['status' => 0, 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $params = $this->validate($request, [
            'name' => 'required|string'
        ]);

        $data = Users::query()->find($id);
        if (!$data) {
            return response()->json(['status' => -1, 'message' => "记录不存在"]);
        }
        $data->update($params);
        return response()->json(['status' => 0, 'message' => "更新成功"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
