<?php

namespace App\Http\Controllers;

use App\Models\Manager;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    // 登录
    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $params = $this->validate($request, [
            'account' => 'required',
            'password' => 'required'
        ]);

        $manager = Manager::query()->where([
            'account' => trim($params['account']),
        ])->first(['id', 'account', 'name', 'password', 'email', 'is_super']);
        if (!$manager) {
            return response()
                ->json(['status' => -1, 'message' => '管理员不存在']);
        }
        if (!Hash::check($params['password'], $manager->password)) {
            return response()
                ->json(['status' => -1, 'message' => '账号或密码错误']);
        }
        $token = $this->jwt($manager);
        unset($manager['password']);
        return response()->json(['status' => 0, 'data' => ['token' => $token, 'manager' => $manager]]);
    }

    protected function jwt(Manager $manager): string
    {
        $payload = [
            'iss' => "example-jwt",
            // Issuer of the token
            'sub' => json_encode(['id' => $manager['id'], 'is_super' => $manager['is_super']], 256),
            // Subject of the token
            'iat' => time(),
            // Time when JWT was issued.
            'exp' => time() + env('JWT_TTL'),
            // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'), env('JWT_ALGO'));
    }
}
