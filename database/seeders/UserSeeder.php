<?php

namespace Database\Seeders;

use App\Models\Users;
use Faker\Factory;
use http\Client\Curl\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factory = Factory::create("zh_CN");

        for ($i = 0; $i <= 10; $i++) {
            $email = $factory->email;
            Users::query()->updateOrCreate(['email' => $email], [
                'name' => $factory->name,
                'email' => $email
            ]);
        }
    }
}
