<?php

namespace Database\Seeders;

use App\Models\Manager;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Manager::query()->firstOrCreate(
            ['account' => "admin"],
            [
                'account' => "admin",
                'name' => "zhimma",
                'password' => Hash::make("zhimma@admin"),
                'is_super' => 1,
                'email' => "18710839146@163.com"
            ]
        );
        Manager::query()->firstOrCreate(
            ['account' => "manager"],
            [
                'account' => "manager",
                'name' => "manager",
                'password' => Hash::make("manager@admin"),
                'is_super' => 0,
                'email' => "manager@163.com"
            ]
        );
    }
}
