<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('managers', function (Blueprint $table) {
            $table->id();
            $table->char("account", 16)->nullable(false)->default("")->comment("账号");
            $table->char("password", 64)->nullable(false)->default("")->comment("密码");
            $table->tinyInteger("is_super")->nullable(false)->default(0)->comment("是否管理员：0-否，1-是");
            $table->string('name', 32)->nullable(false)->default('')->comment("管理员名称");
            $table->string('email', 64)->nullable(false)->default('')->comment("邮箱地址");
            $table->timestamps();
            $table->softDeletes();
            $table->index("account");
            $table->index("email");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('managers');
    }
}
